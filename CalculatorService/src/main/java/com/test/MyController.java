package com.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operation")
public class MyController {

	@GetMapping("/add/{x}/{y}")
	public Response add(@PathVariable int x,@PathVariable int y) 
	{
		return new Response(x,y,x+y);
		
	}
	@GetMapping("/sub/{x}/{y}")
	public Response subtract(@PathVariable int x,@PathVariable int y) 
	{
		return new Response(x,y,x-y);
		
	}
	@GetMapping("/mul/{x}/{y}")
	public Response Multiply(@PathVariable int x,@PathVariable int y) 
	{
		return new Response(x,y,x*y);
		
	}
	@GetMapping("/div/{x}/{y}")
	public Response Division(@PathVariable int x,@PathVariable int y) 
	{
		return new Response(x,y,x/y);
		
	}
	
	
	
}
